﻿using Xamarin.Forms.Platform.iOS;
using Xamarin.Forms;
using UIKit;
using LikwidAR;
using LikwidAR.iOS;

[assembly: ExportRenderer(typeof(CustomARView), typeof(CustomARViewRendered))]
namespace LikwidAR.iOS
{
	public class CustomARViewRendered : PageRenderer
	{
		public UIWindow Window { get; set; }
		protected override void OnElementChanged(VisualElementChangedEventArgs e)
		{
			base.OnElementChanged(e);
			ARViewController viewController = new ARViewController(this);
			UINavigationController navController = new UINavigationController(viewController);
			Window = new UIWindow(UIScreen.MainScreen.Bounds);
			Window.RootViewController = navController;
			Window.MakeKeyAndVisible();
		}
	}
}
