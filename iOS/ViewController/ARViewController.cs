﻿using System;
using UIKit;
using Foundation;
using Wikitude.Architect;
using CoreMotion;
using CoreLocation;
using System.Json;
using System.Collections.Generic;
using System.Linq;

namespace LikwidAR.iOS
{
	public class ARViewController : UIViewController
	{
		protected WTArchitectView arView;
		protected WTNavigation navigation;
		protected ArchitectViewDelegate architectViewDelegate;
		public delegate void Action<WTStartupConfiguration>(WTStartupConfiguration startUpConfig);

		public CustomARViewRendered customARViewRendered;
		public LocationManager locationManager;
		public CLLocation userLocation;
		public JsonArray poiData;
		public string jsonData;

		public ARViewController() : base()
		{
			//this.locationManager = new LocationManager();
			this.poiData = new JsonArray();
		}

		public ARViewController(CustomARViewRendered customARViewRendered) 
		{
			//this.locationManager = new LocationManager();
			this.customARViewRendered = customARViewRendered;
			this.poiData = new JsonArray();
		}

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();

			if (UIDevice.CurrentDevice.CheckSystemVersion(7, 0))
			{
				EdgesForExtendedLayout = UIRectEdge.None;
			}

			var requiredFeatures = WTFeatures.Geo;
			var error = new NSError();
			if (WTArchitectView.IsDeviceSupportedForRequiredFeatures(requiredFeatures, out error))
			{
				//Instanciate ArchitectView and set Wikitude SDK Licence Key
				var motion = new CMMotionManager();
				arView = new WTArchitectView(UIScreen.MainScreen.Bounds, motion);
				arView.SetLicenseKey(Constants.WIKITUDE_SDK_KEY);

				//register Architect View Delegate to Architect View
				this.architectViewDelegate = new ArchitectViewDelegate(this.customARViewRendered);
				this.arView.Delegate = architectViewDelegate;

				//Load the AR View
				var	absoluteWorldUrl = NSBundle.MainBundle.BundleUrl.AbsoluteString + "ArchitectWorld" + "/index.html";
				var u = new NSUrl(absoluteWorldUrl);
				arView.LoadArchitectWorldFromURL(u, requiredFeatures);
				View.AddSubview(arView);
			}
			else
			{
				var adErr = new UIAlertView("Unsupported Device", "This device is not capable of running ARchitect Worlds. Requirements are: iOS 5 or higher, iPhone 3GS or higher, iPad 2 or higher. Note: iPod Touch 4th and 5th generation are only supported in WTARMode_IR.", null, "OK", null);
				adErr.Show();
			}
		}

		public override void ViewDidAppear(bool animated)
		{
			base.ViewDidAppear(animated);

			if (arView != null)
			{
				arView.Start((startupConfiguration) =>
		   		{
					   startupConfiguration.CaptureDevicePosition = AVFoundation.AVCaptureDevicePosition.Back;
		   		}, (isRunning, error) =>
				   {
			   		if (isRunning)
			   		{
						   Console.WriteLine("Wikitude SDK version " + WTArchitectView.SDKVersion + " is running.");
			   		}
			   		else
			   		{
				   		Console.WriteLine("Unable to start Wikitude SDK. Error: " + error.LocalizedDescription);
			   		}
		   		});

				this.jsonData = "[{\"Id\":1,\"Type\":\"Image\",\"Title\":\"SICK TRICK CONTEST\",\"Thumbnail\":\"./sicktrickcontest.png\",\"Emoji\":\"./emojichallenge.png\",\"Latitude\":41.3874638935,\"Longitude\":2.144473999995,\"Altitude\":23},{\"Id\":2,\"Type\":\"Image\",\"Title\":\"SELFIE EVENT\",\"Thumbnail\":\"file:///assets/temp/dropthmbnail2.png\",\"Emoji\":\"file:///assets/temp/emojiselfie.png\",\"Latitude\":41.36746905,\"Longitude\":2.148970499999995,\"Altitude\":27},{\"Id\":3,\"Type\":\"Video\",\"Title\":\"RACE EVENT\",\"Thumbnail\":\"file:///assets/temp/Drop32x32.png\",\"Emoji\":\"file:///assets/temp/emojirace.png\",\"Latitude\":41.3974605,\"Longitude\":2.1689799999995,\"Altitude\":35},{\"Id\":4,\"Type\":\"Image\",\"Title\":\"CHALLENGE SELFIE\",\"Thumbnail\":\"file:///assets/temp/challengeselfie.png\",\"Emoji\":\"file:///assets/temp/emojiselfie.png\",\"Latitude\":41.317405,\"Longitude\":2.188979999995,\"Altitude\":40}]";

				Console.WriteLine(this.jsonData);

				var js = "World.loadPoisFromJsonData(" + this.jsonData + ")";

				this.arView.CallJavaScript(js);

				//locationManager.StartLocationUpdates();

				//locationManager.LocationUpdated += HandleLocationChanged;
			}
		}

		public override void ViewWillDisappear(bool animated)
		{
			base.ViewWillDisappear(animated);

			if (arView != null)
				arView.Stop();
		}

		public void HandleLocationChanged(object sender, LocationUpdatedEventArgs e)
		{
			// Handle foreground updates
			CLLocation location = e.Location;
			if (userLocation == null)
			{
				this.userLocation = new CLLocation();
				this.userLocation = location;
				//Load new POIs 
			}
			else 
			{
				/* if(Number of Different POIs > reference value)
				 * {
				 * 		Calculate the new set of POIs;
				 * 		Load POI from Json (in JS);
				 * }
				 */

				if (GeoUtils.Distance(userLocation, location) > 10) 
				{
					// Update user to distance in js
				}
			}

			Console.WriteLine(location.Altitude + " meters");
			Console.WriteLine(location.Coordinate.Longitude.ToString());
			Console.WriteLine(location.Coordinate.Latitude.ToString());

			Console.WriteLine("foreground updated");
			Console.WriteLine(arView.IsUsingInjectedLocation);
			this.arView.UseInjectedLocation = true;
			Console.WriteLine(arView.IsUsingInjectedLocation);
			this.arView.InjectLocation(location.Coordinate.Latitude, location.Coordinate.Longitude, location.Altitude, 1);
			Console.WriteLine(arView.IsUsingInjectedLocation);
		}
}
	
}
