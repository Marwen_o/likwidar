﻿using System;

using Xamarin.Forms;

namespace LikwidAR
{
	public partial class ARView : ContentPage
	{
		public ARView()
		{
			Label header = new Label
			{
				Text = "Populating Drops Page",
				FontSize = Device.GetNamedSize(NamedSize.Large, typeof(Label)),
				HorizontalOptions = LayoutOptions.Center
			};

			Button button = new Button
			{
				Text = "View Near Drops",
				FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Button)),
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.CenterAndExpand
			};

			button.Clicked += buttonClicked;

			// Accomodate iPhone status bar.
			this.Padding = new Thickness(10, Device.OnPlatform(20, 0, 0), 10, 5);

			// Build the page.
			this.Content = new StackLayout
			{
				Children =
				{
					header,
					button
				}
			};
		}
		void buttonClicked(Object s, EventArgs e)
		{
			Navigation.PushModalAsync(new CustomARView());
		}
	}
}
