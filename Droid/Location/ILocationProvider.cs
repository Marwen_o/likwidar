﻿namespace LikwidAR.Droid
{
	public interface ILocationProvider
	{
		void OnResume();

		void OnPause();
	}
}

